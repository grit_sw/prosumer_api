"""
	Module for interacting with the database
"""
from uuid import uuid4

import arrow
from flask import current_app as app
from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError

from api import db
from api.mixins import KafkaMixin
from api.models.base import Base
from logger import logger

prosumer_group_client_admins = db.Table('facilities',
	db.Column('prosumer_group_id', db.String(100), db.ForeignKey('prosumer_group.group_id')),
	db.Column('prosumer_client_admin_id', db.String(100), db.ForeignKey('prosumer_client_admins.client_admin_id')),
	# db.UniqueConstraint('prosumer_group_id', 'prosumer_client_admin_id', name='prosumer_group_id_prosumer_client_admin_id_unique_pairs'),
)


class ProsumerClass(Base):
	"""
		Prosumer classes include:
		- Producers
		- Consumers
	"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	prosumer_class_id = db.Column(db.String(100), unique=True, nullable=False)
	prosumer_types = db.relationship('ProsumerType', backref='prosumer_class', cascade='all, delete, delete-orphan', lazy='dynamic')

	def __init__(self, name):
		self.name = name.title()
		self.prosumer_class_id = str(uuid4())

	@staticmethod
	def create(class_name):
		new = ProsumerClass(name=class_name)
		try:
			db.session.add(new)
			db.session.commit()
			db.session.refresh(new)
			return new.to_dict()
		except Exception as e:
			db.session.rollback()
			logger.exception(e)
			return {}

	def edit(self, class_name):
		self.name = class_name.title()
		try:
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
			return self.to_dict()
		except Exception as e:
			db.session.rollback()
			logger.exception(e)
			return {}

	@staticmethod
	def init_types():
		names = [
			'Consumer',
			'Producer',
		]
		for name in names:
			if not ProsumerClass.get_one(name=name):
				new_class = ProsumerClass(
					name=name,
					)
				db.session.add(new_class)
		db.session.commit()
		return ProsumerClass.get_all()

	@staticmethod
	def get_one(class_id=None, name=name):
		if class_id:
			prosumer_class = ProsumerClass.query.filter_by(prosumer_class_id=class_id).first()
			return prosumer_class
		elif name:
			name = name.title()
			prosumer_class = ProsumerClass.query.filter_by(name=name).first()
			return prosumer_class
		else:
			raise Exception('Name or ID required.')

	@staticmethod
	def get_consumer():
		name = 'Consumer'
		return ProsumerClass.query.filter_by(name=name).first()

	@staticmethod
	def get_producer():
		name = 'Producer'
		return ProsumerClass.query.filter_by(name=name).first()

	@staticmethod
	def get_all():
		all_prosumer_classes = ProsumerClass.query.order_by(ProsumerClass.name.asc()).all()
		if all_prosumer_classes:
			prosumer_classes = [prosumer_class.to_dict() for prosumer_class in all_prosumer_classes]
			return prosumer_classes
		return []

	def delete_one(self):
		db.session.delete(self)
		db.session.commit()

	def to_dict(self):
		type_dict = {
			'prosumer_class_name': self.name,
			'prosumer_class_id': self.prosumer_class_id
		}
		return type_dict


class ProsumerType(Base):
	"""
		Prosumer types include:
		- Single Phase Consumer
		- Three Phase Consumer
		- Single Phase Generator
		- Three Phase Generator
		- Single Phase Grid
		- Three Phase Grid
		- Single Phase Inverter
		- Three Phase Inverter
	"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	prosumer_type_id = db.Column(db.String(100), unique=True, nullable=False)
	prosumer_class_id = db.Column(db.String(70), db.ForeignKey('prosumer_class.prosumer_class_id'))
	prosumers = db.relationship('Prosumer', backref='prosumer_type', lazy='dynamic', cascade="all, delete-orphan")

	def __init__(self, name, class_obj):
		self.name = name.title()
		self.prosumer_class = class_obj
		self.prosumer_type_id = str(uuid4())

	@staticmethod
	def create(type_name, class_obj):
		new = ProsumerType(name=type_name, class_obj=class_obj)
		try:
			db.session.add(new)
			db.session.commit()
			db.session.refresh(new)
			return new.to_dict()
		except Exception as e:
			db.session.rollback()
			logger.exception(e)
			return None

	def edit(self, type_name, prosumer_class):
		self.name = type_name.title()
		self.prosumer_class = prosumer_class
		try:
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
			return self.to_dict()
		except Exception as e:
			db.session.rollback()
			logger.exception(e)
			return None

	@staticmethod
	def init_types():
		names = [
			('Single Phase Consumer', 'Consumer'),
			('Three Phase Consumer', 'Consumer'),
			('Single Phase Generator', 'Producer'),
			('Three Phase Generator', 'Producer'),
			('Single Phase Grid', 'Producer'),
			('Three Phase Grid', 'Producer'),
			('Single Phase Inverter', 'Producer'),
			('Three Phase Inverter', 'Producer'),
		]
		for item in names:
			name = item[0]
			if not ProsumerType.get_one(name=name):
				class_name = item[1]
				prosumer_class = ProsumerClass.get_one(name=class_name)
				if not prosumer_class:
					class_id = ProsumerClass.create(class_name)['prosumer_class_id']
					class_obj = ProsumerClass.get_one(class_id=class_id)
				else:
					class_obj = prosumer_class

				new_type = ProsumerType(
					name=name,
					class_obj=class_obj
					)
				db.session.add(new_type)
		db.session.commit()
		return ProsumerType.get_all()

	@staticmethod
	def get_all():
		all_prosumer_types = ProsumerType.query.order_by(ProsumerType.name.asc()).all()
		if all_prosumer_types:
			prosumer_types = [prosumer_type.to_dict() for prosumer_type in all_prosumer_types]
			return prosumer_types
		return []

	def delete_one(self):
		db.session.delete(self)
		db.session.commit()

	@staticmethod
	def get_one(type_id=None, name=name):
		if type_id:
			prosumer_type = ProsumerType.query.filter_by(prosumer_type_id=type_id).first()
			return prosumer_type
		elif name:
			name = name.lower().replace(' ', '_')
			prosumer_type = ProsumerType.query.filter_by(name=name).first()
			return prosumer_type
		else:
			raise Exception('Name or ID required.')

	def switch_prosumer_type(self, state, facility_id, kafka_topic, use_avro=False):
		for prosumer in self.prosumers:
			prosumer.switch_state = state
			db.session.add(prosumer)
		switch_dict = {
			'sc': 103,
			'kafka_topic': kafka_topic,
			'use_avro': use_avro,
			'fId': facility_id,
			'ptId': self.prosumer_type_id,
			'conn': state,
			't': str(arrow.now('Africa/Lagos')),
		}
		db.session.commit()
		return switch_dict

	def get_data(self):
		return self.to_dict()

	def to_dict(self):
		type_dict = {
			'prosumer_type_name': self.name.title().replace("_", " "),
			'prosumer_class_name': self.prosumer_class.name,
			'prosumer_type_id': self.prosumer_type_id,
			'prosumer_class_id': self.prosumer_class.prosumer_class_id,
			'prosumer_type_url_name': self.name.lower().replace(" ", "-").replace("_", "-"),
		}
		return type_dict


class ProsumerClientAdmins(Base):
	id = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	client_admin_id = db.Column(db.String(100), unique=True, nullable=False)

	def __init__(self, client_admin_id):
		self.client_admin_id = client_admin_id

	@staticmethod
	def create(client_admin):
		client_admin = ProsumerClientAdmins(
			client_admin_id=client_admin,
			)
		try:
			db.session.add(client_admin)
			db.session.commit()
			db.session.refresh(client_admin)
			return client_admin
		except Exception as e:
			logger.exception(e)
			db.session.rollback()
			return False

	@staticmethod
	def get_one(client_admin=None):
		client_admin = ProsumerClientAdmins.query.filter_by(client_admin_id=client_admin).first()
		return client_admin

	def to_dict(self):
		admin_dict = {
			'id': self.client_admin_id,
		}
		return admin_dict


class ProsumerGroup(Base, KafkaMixin):
	"""
		Prosumer Group Table.
	"""
	id = db.Column(db.Integer, autoincrement=True, unique=True, primary_key=True)
	name = db.Column(db.String(100), unique=False, nullable=False)
	group_id = db.Column(db.String(100), unique=True, nullable=False)
	facility_id = db.Column(db.String(100), unique=False, nullable=False)
	prosumers = db.relationship('Prosumer', backref='prosumer_group', lazy='dynamic')
	client_admins = db.relationship('ProsumerClientAdmins', secondary=prosumer_group_client_admins, backref=db.backref('prosumer_group', lazy='dynamic'))

	def __init__(self, name, facility_id):
		self.name = name.title()
		self.facility_id = facility_id
		self.group_id = str(uuid4())

	@staticmethod
	def create(name, facility_id):
		"""
			Create a new prosumer group.
		"""
		new = ProsumerGroup(
			name=name,
			facility_id=facility_id,
		)
		try:
			db.session.add(new)
			db.session.commit()
			db.session.refresh(new)
			return new.to_dict()
		except Exception as e:
			db.session.rollback()
			logger.exception(e)
			return {}

	@staticmethod
	def get_one(group_id=None, name=None, facility_id=None):
		"""
			Helper method to get a prosumer group by Group ID, Name of Facility ID.
		"""
		if group_id:
			prosumer_group = ProsumerGroup.query\
			.filter_by(group_id=group_id)\
			.first()
			return prosumer_group
		if name:
			name = name.title()
			prosumer_group = ProsumerGroup.query\
			.filter_by(name=name)\
			.filter_by(facility_id=facility_id)\
			.first()
			return prosumer_group

	@staticmethod
	def get_all(facility_id):
		"""
			View all prosumer groups in a facility.
		"""
		prosumer_groups = ProsumerGroup.query.filter_by(facility_id=facility_id).all()
		if prosumer_groups:
			all_groups = [prosumer_group.to_dict() for prosumer_group in prosumer_groups]
			return all_groups
		return []

	def edit(self, name):
		"""
			Edit a prosumer group.
		"""
		self.name = name.title()
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def get_one_prosumer(prosumer_id):
		"""
			Wrapper method to get a prosumer by Prosumer ID.
		"""
		return Prosumer.get_one(prosumer_id=prosumer_id)

	def has_prosumer(self, prosumer):
		prosumer_id = prosumer.prosumer_id
		prosumer = self.prosumers.filter_by(prosumer_id=prosumer_id).first()
		return prosumer

	def add_prosumer(self, prosumer):
		"""Method to add a prosumer to a prosumer group"""
		prosumer_group = prosumer.prosumer_group
		if prosumer_group:
			if prosumer_group is not self:
				# Prosumers can only be in one group at a time.
				prosumer_group.prosumers.remove(prosumer)
				db.session.add(prosumer_group)
		if not prosumer in self.prosumers:
			self.prosumers.append(prosumer)
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		return self.to_dict()

	def remove_prosumer(self, prosumer):
		"""Method to remove a prosumer from a prosumer group"""
		if prosumer in self.prosumers:
			self.prosumers.remove(prosumer)
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		return self.to_dict()

	def add_client_admin(self, client_admin):
		if client_admin not in self.client_admins:
			self.client_admins.append(client_admin)
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		return self.to_dict()

	def remove_client_admin(self, client_admin):
		if client_admin in self.client_admins:
			self.client_admins.remove(client_admin)
			db.session.add(self)
			db.session.commit()
			db.session.refresh(self)
		return self.to_dict()

	def has_client_admin(self, client_admin):
		if client_admin in self.client_admins:
			return True
		return False

	@staticmethod
	def group_client_admins(group_id):
		"""
			Method to get all the client admins that can manage a prosumer group.
		"""
		client_admins = ProsumerClientAdmins.query.join(ProsumerClientAdmins.prosumer_group).filter_by(group_id=group_id)
		return client_admins

	def delete(self):
		db.session.delete(self)
		db.session.commit()

	@staticmethod
	def client_admin_prosumer_groups(client_admin_id):
		"""
			Method to get all the prosumer groups a client admin has.
		"""
		prosumer_groups = ProsumerGroup.query.join(ProsumerGroup.client_admins).filter_by(client_admin_id=client_admin_id)
		return prosumer_groups

	def switch_prosumer_group(self, state, kafka_topic, use_avro=False):
		for prosumer in self.prosumers:
			prosumer.switch_state = state
			db.session.add(prosumer)
		switch_dict = {
			'sc': 104,
			'kafka_topic': kafka_topic,
			'use_avro': use_avro,
			'pgId': self.group_id,
			'conn': state,
			't': str(arrow.now('Africa/Lagos')),
		}
		db.session.commit()
		return switch_dict

	def get_data(self):
		return self.to_dict()

	def small_dict(self):
		"""
			Method to view small details of a prosumer group.
		"""
		group_dict = {
			"name": self.name.title(),
			'group_id': self.group_id,
			'client_admins': [
				client_admin.to_dict() for client_admin in
				ProsumerGroup.group_client_admins(
					self.group_id
				).all()
			]
		}
		return group_dict

	def to_dict(self):
		"""
			Method to view details of a prosumer group.
		"""
		group_dict = {
			"name": self.name.title(),
			'group_id': self.group_id,
			'facility_id': self.facility_id,
			'prosumers': [prosumer.small_dict() for prosumer in self.prosumers.all()],
			'client_admins': [
				client_admin.to_dict() for client_admin in
				ProsumerGroup.group_client_admins(
					self.group_id
				).all()
			]
		}
		return group_dict


class UserGroup(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	user_group_id = db.Column(db.String(50), unique=True, nullable=False)
	prosumers = db.relationship('Prosumer', backref='owner', lazy='dynamic')

	def __init__(self, user_group_id):
		self.user_group_id = user_group_id

	@staticmethod
	def create(user_group_id):
		user_group = UserGroup(user_group_id)
		db.session.add(user_group)
		db.session.commit()
		db.session.refresh(user_group)
		return user_group

	@staticmethod
	def get_one(user_group_id):
		return UserGroup.query.filter_by(user_group_id=user_group_id).first()

	@staticmethod
	def get_one_or_create(user_group_id):
		user_group = UserGroup.get_one(user_group_id)
		if not user_group:
			user_group = UserGroup.create(user_group_id)
		return user_group

	@staticmethod
	def get_one_or_create_no_commit(user_group_id):
		user_group = UserGroup.get_one(user_group_id)
		if not user_group:
			user_group = UserGroup(user_group_id)
			db.session.add(user_group)
		return user_group

	def get_data(self):
		return self.to_dict()

	def to_dict(self):
		data = {
			'user_group_id': self.user_group_id,
			'prosumer_count': len(self.prosumers.all()),
		}
		return data


class Probe(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	# numerical address (number) of the shop
	name = db.Column(db.String(100), unique=False, nullable=False)
	prosumer_id = db.Column(db.String(70), db.ForeignKey('prosumer.prosumer_id'))

	def __init__(self, name):
		self.name = name

	@staticmethod
	def get_one(name):
		probe = Probe.query.filter_by(name=name).first()
		return probe

	@staticmethod
	def get_one_or_create_no_commit(probe_name):
		probe = Probe.get_one(probe_name)
		if not probe:
			probe = Probe(
				name=probe_name
			)
		db.session.add(probe)
		return probe

	def send_to_kafka(self):
		# get all the prosumers on this probe and send them to kafka
		prosumers = Prosumer.query.join(Probe).filter_by(name=self.name).all()
		for prosumer in prosumers:
			prosumer.send_prosumer_to_kafka()
		return self

	def to_dict(self):
		response = {
			'name': self.name
		}
		return response


class Subnet(Base):
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	subnet_id = db.Column(db.String(100), unique=False, nullable=False)
	prosumer_id = db.Column(db.String(70), db.ForeignKey('prosumer.prosumer_id'))

	def __init__(self, subnet_id):
		self.subnet_id = subnet_id

	@staticmethod
	def get_one(subnet_id):
		subnet = Subnet.query.filter_by(subnet_id=subnet_id).first()
		return subnet

	@staticmethod
	def get_one_or_create_no_commit(subnet_id):
		subnet = Subnet.get_one(subnet_id)
		if not subnet:
			subnet = Subnet(
				subnet_id=subnet_id
			)
		db.session.add(subnet)
		return subnet

	def to_dict(self):
		response = {
			'subnet_id': self.subnet_id
		}
		return response


class Prosumer(Base, KafkaMixin):
	"""
		Metering devices.
		# The probe ID is the ID of either the grit meter or a third party meter being used.
	"""
	id = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	# numerical address (number) of the shop
	name = db.Column(db.String(100), unique=False, nullable=False)
	prosumer_type_id = db.Column(db.String(70), db.ForeignKey('prosumer_type.prosumer_type_id'))
	group_id = db.Column(db.String(100), db.ForeignKey('prosumer_group.group_id'))
	configuration_id = db.Column(db.String, unique=True, nullable=False)
	prosumer_id = db.Column(db.String, unique=True, nullable=False)
	device_id = db.Column(db.String, unique=True, nullable=False)
	prosumer_url = db.Column(db.String, unique=True, nullable=False)
	user_group_id = db.Column(db.String(70), db.ForeignKey('user_group.user_group_id'))
	facility_id = db.Column(db.String(50), unique=False, nullable=False)
	assigned = db.Column(db.Boolean, default=False)
	was_assigned = db.Column(db.Boolean, default=False)
	deleted = db.Column(db.Boolean, default=False)
	connected = db.Column(db.Boolean, default=False)
	switch_state = db.Column(db.Boolean, default=False)
	date_assigned = db.Column(db.DateTime)
	date_unassigned = db.Column(db.DateTime)
	buy_order_id = db.Column(db.String, unique=False, nullable=True)
	sell_order_id = db.Column(db.String, unique=False, nullable=True)
	keypad_id = db.Column(db.String(50), unique=True, nullable=True)
	probes = db.relationship('Probe', backref='prosumer', lazy='dynamic')
	subnets = db.relationship('Subnet', backref='prosumer', lazy='dynamic')


	def __repr__(self):
		return '<Prosumer = {}>'.format(self.prosumer_id)

	def __init__(self, name, device_id, prosumer_url, configuration_id, prosumer_type, user_group, prosumer_group, facility_id):
		"""Init end consumer"""
		self.name = name.title()
		self.prosumer_id = str(uuid4())
		self.prosumer_type = prosumer_type
		self.configuration_id = configuration_id
		self.prosumer_group = prosumer_group
		self.facility_id = facility_id
		self.device_id = device_id
		self.prosumer_url = prosumer_url
		self.owner = user_group
		self.date_assigned = arrow.now().datetime
		self.assigned = True

	@staticmethod
	def create(prosumer_name, device_id, prosumer_url, prosumer_type_id, user_group_id, configuration_id, prosumer_group_id, facility_id, subnet_ids, probe_names):
		new_prosumer = Prosumer(
			name=prosumer_name,
			device_id=device_id,
			prosumer_url=prosumer_url,
			facility_id=facility_id,
			configuration_id=configuration_id,
			prosumer_type=ProsumerType.get_one(type_id=prosumer_type_id),
			user_group=UserGroup.get_one_or_create_no_commit(user_group_id),
			prosumer_group=ProsumerGroup.get_one(group_id=prosumer_group_id, facility_id=facility_id),
		)
		db.session.autoflush = False
		db.session.add(new_prosumer)
		new_prosumer.add_subnets(subnet_ids)
		new_prosumer.add_probes(probe_names)
		kafka_topic = app.config.get('PROSUMER_UPDATE_TOPIC')
		new_prosumer_kafka_data = new_prosumer.kafka_dict(kafka_topic)
		new_prosumer_data = new_prosumer.to_dict()
		try:
			db.session.commit()
		except IntegrityError as e:
			db.session.rollback()
			logger.critical(e)
			KafkaMixin.notify_kafka(new_prosumer_kafka_data)
			return new_prosumer_data
		db.session.refresh(new_prosumer)
		return new_prosumer.to_dict()

	@staticmethod
	def create_bulk(prosumers):
		created_prosumers = []
		kafka_data = []
		logger.info(f"Prosumer input - {prosumers}")
		for prosumer in prosumers:
			prosumer_obj = Prosumer.get_one(device_id=prosumer['device_id'])
			if prosumer_obj:
				logger.info(f"conflict 1 - {created_prosumers}")
				created_prosumers.append(prosumer_obj.to_dict())
				continue

			new_prosumer = Prosumer(
				name=prosumer['prosumer_name'],
				device_id=prosumer['device_id'],
				configuration_id=prosumer['configuration_id'],
				prosumer_url=prosumer['prosumer_url'],
				facility_id=prosumer['facility_id'],
				prosumer_type=ProsumerType.get_one(type_id=prosumer['prosumer_type_id']),
				user_group=UserGroup.get_one_or_create_no_commit(prosumer['user_group_id']),
				prosumer_group=ProsumerGroup.get_one(group_id=prosumer['prosumer_group_id'], facility_id=prosumer['facility_id']),
			)
			db.session.autoflush = False
			db.session.add(new_prosumer)
			new_prosumer.add_subnets(prosumer['subnet_ids'])
			new_prosumer.add_probes(prosumer['probe_names'])
			db.session.add(new_prosumer)
			kafka_topic = app.config.get('PROSUMER_UPDATE_TOPIC')
			new_prosumer_kafka_data = new_prosumer.kafka_dict(kafka_topic)
			kafka_data.append(new_prosumer_kafka_data)
			new_prosumer_data = new_prosumer.to_dict()
			created_prosumers.append(new_prosumer_data)
			logger.info(f"Created prosumers 1 - {created_prosumers}")
		try:
			db.session.commit()
		except IntegrityError as e:
			db.session.rollback()
			logger.critical(e)
			for data in kafka_data:
				KafkaMixin.notify_kafka(data)
			return created_prosumers
		logger.info(f"Created prosumers 2 - {created_prosumers}")
		return created_prosumers

	def name_exists(self, name):
		name = name.title()
		return Prosumer.query.filter_by(user_group_id=self.user_group_id).filter_by(name=name).first()

	def add_subnets(self, subnet_ids):
		for subnet_id in subnet_ids:
			subnet = Subnet.get_one_or_create_no_commit(subnet_id)
			if subnet not in self.subnets.all():
				self.subnets.append(subnet)
		db.session.add(self)
		return self

	def add_probes(self, probe_names):
		for probe_name in probe_names:
			probe = Probe.get_one_or_create_no_commit(probe_name)
			if probe not in self.probes.all():
				self.probes.append(probe)
		db.session.add(self)
		return self

	def edit_probes(self, probe_names):
		new_probe_names = set(probe_names)
		existing_probe_names = set([probe.name for probe in self.probes.all()])
		deleted_probes = existing_probe_names.difference(new_probe_names)
		for probe_name in deleted_probes:
			probe = Probe.get_one(probe_name)
			if probe in self.probes:
				self.probes.remove(probe)
				db.session.add(self)
		self.add_probes(probe_names)
		db.session.add(self)
		return self

	def edit_subnets(self, subnet_ids):
		new_subnet_ids = set(subnet_ids)
		existing_subnet_ids = set([subnet.subnet_id for subnet in self.subnets.all()])
		deleted_subnets = existing_subnet_ids.difference(new_subnet_ids)
		for subnet_subnet_id in deleted_subnets:
			subnet = Probe.get_one(subnet_subnet_id)
			if subnet in self.subnets:
				self.subnets.remove(subnet)
				db.session.add(self)
		self.add_subnets(subnet_ids)
		db.session.add(self)
		return self

	def edit(self, prosumer_name, device_id, prosumer_url, prosumer_type_id, user_group_id, prosumer_group_id, facility_id, subnet_ids, probe_names):
		db.session.autoflush = False
		self.name = prosumer_name.title()
		self.facility_id = facility_id
		self.device_id = device_id
		self.prosumer_url = prosumer_url
		self.prosumer_type = ProsumerType.get_one(type_id=prosumer_type_id)
		self.prosumer_group = ProsumerGroup.get_one(group_id=prosumer_group_id, facility_id=facility_id)
		self.owner = UserGroup.get_one_or_create_no_commit(user_group_id)
		self.edit_probes(probe_names)
		self.edit_subnets(subnet_ids)
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	@staticmethod
	def search_for(search_term, size):
		prosumers = Prosumer.query.filter(or_(
				Prosumer.name.like('%' + search_term.title() + '%'),
				Prosumer.user_group_id.like('%' + search_term + '%'),
				)
			)
		prosumers = prosumers.order_by(Prosumer.name).paginate(
			1, size, False)
		all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def exists(prosumer_id):
		_prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if _prosumer is None:
			return False
		return True

	@staticmethod
	def get_by_facility(facility_id):
		prosumer = Prosumer.query.filter_by(
			facility_id=facility_id).filter_by(deleted=False).first()
		return prosumer

	@staticmethod
	def prosumers_in_subnet(page, subnet_id):
		prosumers = Prosumer.query\
		.join(Subnet)\
		.filter_by(subnet_id=subnet_id)\
		.paginate(page, app.config['USERS_PER_PAGE'], False)
		if not prosumers.items:
			return [], False, False
		else:
			all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def consumers_in_subnet(page, subnet_id):
		consumer = ProsumerClass.get_consumer()
		if not consumer:
			return [], False, False
		consumer_id = consumer.prosumer_class_id
		prosumers = Prosumer.query\
		.join(ProsumerType)\
		.filter_by(prosumer_class_id=consumer_id)\
		.join(Subnet)\
		.filter_by(subnet_id=subnet_id)\
		.paginate(page, app.config['USERS_PER_PAGE'], False)
		if not prosumers.items:
			return [], False, False
		else:
			all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def producers_in_subnet(page, subnet_id):
		producer = ProsumerClass.get_producer()
		if not producer:
			return [], False, False
		producer_id = producer.prosumer_class_id
		prosumers = Prosumer.query\
		.join(ProsumerType)\
		.filter_by(prosumer_class_id=producer_id)\
		.join(Subnet)\
		.filter_by(subnet_id=subnet_id)\
		.paginate(page, app.config['USERS_PER_PAGE'], False)
		if not prosumers.items:
			return [], False, False
		else:
			all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def get_by_user_group(user_group_id):
		prosumer = Prosumer.query.filter_by(
			user_group_id=user_group_id).filter_by(deleted=False).first()
		return prosumer

	@staticmethod
	def get_one(name=None, user_group_id=None, prosumer_id=None, device_id=None,
		buy_order_id=None, sell_order_id=None, deleted=False):
		if name is not None:
			name = name.replace(' ', '_').title()
			prosumer = Prosumer.query.filter_by(
				name=name).filter_by(deleted=deleted).first()
			return prosumer
		if user_group_id is not None:
			prosumer = Prosumer.query.filter_by(
				user_group_id=user_group_id).filter_by(deleted=deleted).first()
			return prosumer
		if prosumer_id is not None:
			prosumer = Prosumer.query.filter_by(
				prosumer_id=prosumer_id).filter_by(deleted=deleted).first()
			return prosumer
		if device_id is not None:
			prosumer = Prosumer.query.filter_by(
				device_id=device_id).filter_by(deleted=deleted).first()
			return prosumer
		if buy_order_id is not None:
			prosumer = Prosumer.query.filter_by(
				buy_order_id=buy_order_id).filter_by(deleted=deleted).first()
		if sell_order_id is not None:
			prosumer = Prosumer.query.filter_by(
				sell_order_id=sell_order_id).filter_by(deleted=deleted).first()
			return prosumer

	@staticmethod
	def get_all_prosumers(name=None, user_group_id=None, prosumer_id=None,
		buy_order_id=None, sell_order_id=None, deleted=False):
		if name is not None:
			name = name.replace(' ', '_').title()
			prosumer = Prosumer.query.filter_by(
				name=name).filter_by(deleted=deleted).all()
			return prosumer
		if user_group_id is not None:
			prosumer = Prosumer.query.filter_by(
				user_group_id=user_group_id).filter_by(deleted=deleted).all()
			return prosumer
		if prosumer_id is not None:
			prosumer = Prosumer.query.filter_by(
				prosumer_id=prosumer_id).filter_by(deleted=deleted).all()
			return prosumer
		if buy_order_id is not None:
			prosumer = Prosumer.query.filter_by(
				buy_order_id=buy_order_id).filter_by(deleted=deleted).all()
			return prosumer
		if sell_order_id is not None:
			prosumer = Prosumer.query.filter_by(
				sell_order_id=sell_order_id).filter_by(deleted=deleted).all()
			return prosumer

	@staticmethod
	def get_prosumer_by_type(page, type_id):
		prosumers = Prosumer.query\
		.filter_by(prosumer_type_id=type_id)\
		.filter_by(deleted=False)\
		.order_by(Prosumer.id.asc())\
		.paginate(page, app.config['USERS_PER_PAGE'], False)
		if not prosumers.items:
			return [], False, False
		else:
			all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def get_prosumer_by_type_by_facility(page, type_id, facility_id):
		prosumers = Prosumer.query\
		.filter_by(facility_id=facility_id)\
		.filter_by(prosumer_type_id=type_id)\
		.filter_by(deleted=False).order_by(Prosumer.id.asc())\
		.paginate(page, app.config['USERS_PER_PAGE'], False)
		if not prosumers.items:
			return [], False, False
		else:
			all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def get_prosumer_by_prosumer_group(page, group_id):
		prosumers = Prosumer.query.filter_by(
			group_id=group_id,
			).filter_by(deleted=False).order_by(Prosumer.id.asc()).paginate(
			page, app.config['USERS_PER_PAGE'], False
		)
		if not prosumers.items:
			return [], False, False
		else:
			all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	def delete_one(self, destroy=False):
		if destroy:
			db.session.delete(self)
			db.session.commit()
			return
		self.deleted = True
		db.session.add(self)
		db.session.commit()

	@staticmethod
	def user_prosumers(page, user_group_id):
		prosumers = Prosumer.query.filter_by(
			user_group_id=user_group_id).filter_by(deleted=False).paginate(
			page, app.config['USERS_PER_PAGE'], False)

		if not prosumers.items:
			return [], False, False
		all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def client_admin_prosumers(page, client_admin_id):
		"""
			Get all the prosumers a client admin has access to.
			The sqlalchemy query can most likely be improved. Ive just not figured it out yet.
			Client admins have direct no relationship with prosumers.
			Client admins have access to prosumer groups.
			Client admin and prosumer group is a many-to-many relationship.
			Prosumer group and prosumer is a one to many relationship.
		"""
		# Query 1
		prosumer_groups = ProsumerGroup.client_admin_prosumer_groups(client_admin_id).all()

		prosumer_groups = [prosumer_group.group_id for prosumer_group in prosumer_groups]

		# Query 2
		prosumers = Prosumer.query.filter_by(deleted=False).filter(Prosumer.group_id.in_(prosumer_groups)).order_by(Prosumer.id).paginate(page, app.config['USERS_PER_PAGE'], False)

		if not prosumers.items:
			return [], False, False
		all_prosumers = [prosumer.to_dict() for prosumer in prosumers.items]
		return all_prosumers, prosumers.has_prev, prosumers.has_next

	@staticmethod
	def all(page):
		prosumers_ = Prosumer.query.filter_by(deleted=False).paginate(
			page, app.config['USERS_PER_PAGE'], False)
		if not prosumers_.items:
			return [], False, False

		all_prosumers = [prosumer.to_dict() for prosumer in prosumers_.items]
		return all_prosumers, prosumers_.has_prev, prosumers_.has_next

	@staticmethod
	def all_in_facility(page, facility_id):
		prosumers_ = Prosumer.query.filter_by(deleted=False).filter_by(facility_id=facility_id).paginate(
			page, app.config['USERS_PER_PAGE'], False)
		if not prosumers_.items:
			return [], False, False

		all_prosumers = [prosumer.to_dict() for prosumer in prosumers_.items]
		return all_prosumers, prosumers_.has_prev, prosumers_.has_next

	@staticmethod
	def unassigned(facility_id):
		"""
			Method that obtains a list of PROSUMERS that are not assigned to any user.
		"""
		unmetered = Prosumer.query.filter_by(deleted=False).filter_by(
			facility_id=facility_id).filter_by(assigned=False).all()
		payload = []
		if len(unmetered) >= 1:
			payload = [prosumer.to_dict() for prosumer in unmetered]
		return payload

	@staticmethod
	def assigned_prosumers(facility_id):
		"""
			Method that obtains a list of PROSUMERS that are ASSIGNED to a user.
		"""
		metered = Prosumer.query.filter_by(deleted=False).filter_by(
			facility_id=facility_id).filter_by(assigned=True).all()
		payload = []
		if len(metered) >= 1:
			payload = [prosumer.to_dict() for prosumer in metered]
		return payload

	@staticmethod
	def assign_to_user_group(group_id, prosumer):
		"""Method to add a prosumer to a user group"""
		prosumer.user_group_id = group_id
		prosumer.date_assigned = arrow.now().datetime
		prosumer.assigned = True
		prosumer.was_assigned = False
		db.session.add(prosumer)
		try:
			db.session.commit()
			db.session.refresh(prosumer)
		except IntegrityError as e:
			db.session.rollback(e)
			logger.critical(e)
			raise

		return prosumer.to_dict()

	@staticmethod
	def remove_from_user_group(prosumer):
		"""
			Method to remove a prosumer from a user group
			TODO: Add a column in the user Settings table to enable option
			for assigning multiple prosumers to a user group.
			Currently, users can only be assigned one prosumer.
		"""
		prosumer.date_unassigned = arrow.now().datetime
		prosumer.user_group_id = None
		prosumer.assigned = False
		prosumer.was_assigned = True
		db.session.add(prosumer)
		db.session.commit()
		db.session.refresh(prosumer)
		return prosumer.to_dict()

	@staticmethod
	def remove_all_from_user_group(user_group_id):
		"""
			Method to remove all prosumers from a user group
			TODO: put this method in a thread so that the request can return
			before the completion of this task.
		"""
		prosumers = Prosumer.get_all_prosumers(user_group_id=user_group_id)
		for prosumer in prosumers:
			prosumer.date_unassigned = arrow.now().datetime
			prosumer.user_group_id = None
			prosumer.assigned = False
			prosumer.was_assigned = True
			db.session.add(prosumer)
		db.session.commit()
		return

	@staticmethod
	def reassign_to_user_group(group_id, prosumer):
		"""Method to reassign a prosumer to a different user group"""

		prosumer.user_group_id = group_id
		prosumer.date_assigned = arrow.now().datetime
		prosumer.assigned = True
		prosumer.was_assigned = False
		db.session.add(prosumer)
		try:
			db.session.commit()
			db.session.refresh(prosumer)
		except IntegrityError as e:
			db.session.rollback(e)
			raise

		return prosumer.to_dict()

	def save_buy_order(self, buy_order_id):
		"""
			Method to add an energy plan ID to a prosumer.
			TODO: Add plan to all prosumers of a particular type in a facility
			TODO: Add plan to all prosumers in a prosumer group in a facility
		"""
		if not self.is_a_consumer():
			return None
		self.buy_order_id = buy_order_id
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	def save_sell_order(self, sell_order_id):
		"""
			Method to add an energy plan ID to a prosumer.
			todo: Add plan to all prosumers of a particular type in a facility
			todo: Add plan to all prosumers in a prosumer group in a facility
		"""
		if not self.is_a_producer():
			return None
		self.sell_order_id = sell_order_id
		db.session.add(self)
		db.session.commit()
		db.session.refresh(self)
		return self.to_dict()

	def has_consumer(self):
		prosumers = Prosumer.query.filter_by(user_group_id=self.user_group_id).all()
		has_consumer_ = False
		for prosumer in prosumers:
			if prosumer.prosumer_type.prosumer_class.name == 'Consumer':
				has_consumer_ = True
				break
		return has_consumer_

	def has_producer(self):
		prosumers = Prosumer.query.filter_by(user_group_id=self.user_group_id).all()
		has_producer_ = False
		for prosumer in prosumers:
			if prosumer.prosumer_type.prosumer_class.name == 'Producer':
				has_producer_ = True
				break
		return has_producer_

	def is_a_producer(self):
		try:
			return self.prosumer_type.prosumer_class.name == 'Producer'
		except AttributeError as e:
			logger.exception(e)
			return False

	def is_a_consumer(self):
		try:
			return self.prosumer_type.prosumer_class.name == 'Consumer'
		except AttributeError as e:
			logger.exception(e)
			return False

	@staticmethod
	def assignment_params(facility_id):
		prosumer_params = Prosumer.unassigned(facility_id)
		prosumer_params = [Prosumer.basic_info(
			param) for param in prosumer_params]

		return prosumer_params

	@staticmethod
	def removal_params(facility_id):
		prosumer_params = Prosumer.assigned_prosumers(facility_id)
		prosumer_params = [Prosumer.basic_info(
			param) for param in prosumer_params]

		return prosumer_params

	@staticmethod
	def basic_info(prosumer_dict):
		resp = {}
		resp['prosumer_id'] = prosumer_dict['prosumer_id']
		resp['prosumer_name'] = prosumer_dict['prosumer_name']
		return resp

	@property
	def prosumer_class(self):
		if self.prosumer_type:
			return self.prosumer_type.prosumer_class.to_dict()
		return None

	@property
	def my_client_admins(self):
		if self.prosumer_group:
			client_admins = ProsumerGroup.group_client_admins(self.group_id)
			return [
				client_admin.to_dict()
				for client_admin in
				client_admins
				]
		return []

	def client_admin_list(self):
		if self.prosumer_group:
			client_admins = ProsumerGroup.group_client_admins(self.group_id)
			return [client_admin.client_admin_id for client_admin in client_admins]
		return []

	@property
	def kafka_client_admins(self):
		if self.prosumer_group:
			client_admins = ProsumerGroup.group_client_admins(self.group_id)
			return [
				client_admin.to_dict()['id']
				for client_admin in
				client_admins
				]
		return []

	def start_prosumer_config(self):
		payload = self.config_init()
		KafkaMixin.notify_kafka(payload)

	def config_init(self):
		config_dict = {
			'prosumer_id': self.prosumer_id,
			'kafka_topic': 'prepare-config',
		}
		return config_dict

	def prepare_probe_config(self):
		config = self.probe_config()
		# Send config to kafka
		return config

	def probe_config(self):
		probe_dict = {
			'mid': self.probe.device_id,
			'data': [
				prosumer.prosumer_config() for prosumer in self.probe.prosumers.order_by(Prosumer.name)
			]
		}
		return probe_dict

	def prosumer_config(self):
		prosumer_dict = {
			'ugId': self.user_group_id,
			'sN': self.name.split('-')[1],
			'fId': self.facility_id,
			'prosId': self.prosumer_id,
			'planId': self.buy_order_id,
			'ptId': self.prosumer_type_id,
			'pgId': self.group_id,
		}
		return prosumer_dict

	def switch_prosumer(self, state, kafka_topic, use_avro=False):
		switch_dict = {
			'sc': 101,
			'kafka_topic': kafka_topic,
			'use_avro': use_avro,
			'mid': self.name.split('-')[0],
			'sourceName': self.name.split('-')[1],
			'conn': state,
			't': str(arrow.now('Africa/Lagos')),
		}
		return switch_dict

	def switch_facility(self, state, kafka_topic, use_avro=False):
		switch_dict = {
			'sc': 102,
			'kafka_topic': kafka_topic,
			'use_avro': use_avro,
			'fId': self.facility_id,
			'conn': state,
			't': str(arrow.now('Africa/Lagos')),
		}
		return switch_dict

	@property
	def is_assigned(self):
		return bool(self.user_group_id)

	def get_data(self):
		return self.to_dict()

	def to_dict(self):
		json_prosumer = {
			'id': self.id,
			'prosumer_name': self.name,
			'probe_names': [probe.name for probe in self.probes.all()],
			'user_group_id': self.user_group_id,
			'configuration_id': self.configuration_id,
			'prosumer_id': self.prosumer_id,
			'device_id': self.device_id,
			'buy_order_id': self.buy_order_id,
			'keypad_id': self.keypad_id,
			'connected': self.connected,
			'facility_id': self.facility_id,
			'subnets': [subnet.to_dict() for subnet in self.subnets.all()],
			'assigned': self.is_assigned,
			'was_assigned': self.was_assigned,
			'date_created': str(self.date_created),
			'date_assigned': str(self.date_assigned),
			'date_unassigned': str(self.date_unassigned),
			'client_admins': self.my_client_admins,
			'prosumer_class': self.prosumer_class,
			'prosumer_type': self.prosumer_type.to_dict() if self.prosumer_type else None,
			'prosumer_group': self.prosumer_group.small_dict() if self.prosumer_group else None,
		}
		return json_prosumer

	def small_dict(self):
		json_prosumer = {
			'prosumer_id': self.prosumer_id,
			'user_group_id': self.user_group_id,
			'facility_id': self.facility_id,
			'subnets': [subnet.subnet_id for subnet in self.subnets.all()],
			'device_id': self.device_id,
			'configuration_id': self.configuration_id,
			'local_id': self.name.split('-')[-1],
			'prosumer_name': self.name,
			'assigned': self.is_assigned,
			'probe_names': [probe.name for probe in self.probes.all()],
			'was_assigned': self.was_assigned,
			'date_created': str(self.date_created),
			'client_admins': self.my_client_admins,
			'prosumer_class': self.prosumer_class,
			'prosumer_type': self.prosumer_type.to_dict() if self.prosumer_type else None,
			'prosumer_group': self.prosumer_group.small_dict() if self.prosumer_group else None,
		}
		return json_prosumer

	def my_info(self):
		json_data = {
			'user_group_id': self.user_group_id,
			'has_consumer': self.has_consumer(),
			'has_producer': self.has_producer(),
		}
		return json_data

	def send_prosumer_to_kafka(self):
		kafka_topic = app.config.get('PROSUMER_UPDATE_TOPIC')
		KafkaMixin.notify_kafka(self.kafka_dict(kafka_topic))
		return True

	def kafka_dict(self, kafka_topic, use_avro=True):
		prosumer = {
			'kafka_topic': kafka_topic,
			'use_avro': use_avro,
			'prosumer_name': self.name,
			'prosumer_id': self.prosumer_id,
			'probe_names': [probe.name for probe in self.probes.all()],
			'device_id': self.device_id,
			'configuration_id': self.configuration_id,
			'prosumer_url': self.prosumer_url,
			'facility_id': self.facility_id,
			'user_group_id': self.user_group_id,
			'to_delete': bool(self.deleted),
			'was_assigned': bool(self.was_assigned),
			'client_admins': self.kafka_client_admins,
		}
		return prosumer

	# def cmdctrl_dict(self):
	# 	json_prosumer = {
	# 		'user_group_id': self.user_group_id,
	# 		'prosumer_name': self.name,
	# 		'prosumer_id': self.prosumer_id,
	# 		'device_id': self.device_id,
	# 		'connected': self.connected,
	# 		'assigned': self.is_assigned,
	# 	}
	# 	return json_prosumer


db.event.listen(db.session, 'before_commit', Prosumer.before_commit)
db.event.listen(db.session, 'after_commit', Prosumer.after_commit)
