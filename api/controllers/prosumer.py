"""
	Module that directly interfaces with requests from the web
"""
from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import ProsumerManager, RefreshManager
from api.schema import SellOrderSchema, BuyOrderSchema, NewProsumerSchema, BulkNewProsumerSchema
from logger import logger


prosumer_api = Namespace('Prosumer', description='Api for managing prosumers')


new_prosumer = prosumer_api.model('New Prosumer', {
	'prosumer_name': fields.String(required=False, description='The name of the prosumer. E.g. Shop1, Main House.'),
	'user_group_id': fields.String(required=True, description="The user group assigned to this prosumer."),
	'configuration_id': fields.String(required=False, description="The configuration ID of this prosumer."),
	'device_id': fields.String(required=True, description='The ID of the device. This can be the ID of the grit meter or the serial number of a 3rd party meter.'),
	'prosumer_url': fields.String(required=True, description='The url of the device configuration.'),
	'prosumer_type_id': fields.String(required=True, description='The ID of the type of prosumer this is.'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the group of prosumer this is.'),
	'facility_id': fields.String(required=True, description='The facility ID where this prosumer belongs to.'),
	'subnet_ids': fields.List(fields.String(required=True, description='The subnet ID where this prosumer belongs to.')),
	'probe_names': fields.List(fields.String(required=True, description='The names of probe(s) that have been used with this prosumer.')),
})


bulk_new_prosumer = prosumer_api.model('Bulk New Prosumer', {
	'probe_names': fields.List(fields.Nested(new_prosumer, description='List of prosumers to be created.')),
})


sell_order = prosumer_api.model('Sell Order', {
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
	'sellorder_id': fields.String(required=True, description='The ID of the plan the prosumer is on'),
})


buy_order = prosumer_api.model('Buy Order', {
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
	'buyorder_id': fields.String(required=True, description='The ID of the plan the prosumer is on'),
})


@prosumer_api.route('/new/')
class NewProsumer(Resource):
	@prosumer_api.expect(new_prosumer)
	def post(self):
		"""
				HTTP method to create prosumers
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewProsumerSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.new_prosumer(new_payload)
		return resp, code


@prosumer_api.route('/bulk-new/')
class BulkNewProsumer(Resource):
	@prosumer_api.expect(bulk_new_prosumer)
	def post(self):
		"""
				HTTP method to create prosumers
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = BulkNewProsumerSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
			new_payload['prosumers'] = [dict(prosumer._asdict()) for prosumer in new_payload['prosumers']]
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.new_bulk_prosumer(role_id, new_payload)
		return resp, code


@prosumer_api.route('/edit/<string:prosumer_id>/')
class EditProsumer(Resource):
	@prosumer_api.expect(new_prosumer)
	def post(self, prosumer_id):
		"""
				HTTP method to create prosumers
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = NewProsumerSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		new_payload['prosumer_id'] = prosumer_id
		resp, code = manager.edit(new_payload)
		return resp, code


@prosumer_api.route('/search/')
class Search(Resource):
	"""
			Api for a user to view his prosumer(s)
	"""

	def get(self):
		"""
				HTTP method for a user to view his prosumer(s)
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		search_term = request.args.get('query', type=str)
		size = request.args.get('size', 20, type=int)
		resp, code = manager.search(search_term, size, role_id)
		return resp, code


@prosumer_api.route('/me/')
class MyProsumer(Resource):
	"""
			Api for a user to view his prosumer(s)
	"""

	def get(self):
		"""
				HTTP method for a user to view his prosumer(s)
				@returns: response and status code
		"""
		manager = ProsumerManager()
		group_id = request.cookies.get('group_id')
		user_id = request.cookies.get('user_id')
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.my_prosumers(page, user_id, group_id, role_id)
		return resp, code


@prosumer_api.route('/my-info/')
class MyProsumerInfo(Resource):
	"""
			Api for a user to view the nature of the prosumers installed
	"""

	def get(self):
		"""
				HTTP method for a user to view the nature of the prosumers installed
				@returns: response and status code
		"""
		manager = ProsumerManager()
		group_id = request.cookies.get('group_id')
		resp, code = manager.my_info(group_id)
		return resp, code


@prosumer_api.route('/user/<string:group_id>/')
class UserProsumer(Resource):
	"""
			Api to view the prosumers a user has
	"""

	def get(self, group_id):
		"""
				HTTP method to view the prosumers a user has
				@returns: response and status code
		"""
		manager = ProsumerManager()
		page = request.args.get('page', 1, type=int)
		resp, code = manager.user_prosumers(page, group_id)
		return resp, code


@prosumer_api.route('/get-one/<string:prosumer_id>/')
class OneProsumer(Resource):
	def get(self, prosumer_id):
		"""
				HTTP method to view details of a prosumer
				@param: prosumer_id: ID of the prosumer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		resp, code = manager.get_one(prosumer_id)
		return resp, code


@prosumer_api.route('/get-one-in-facility/<string:prosumer_id>/<string:facility_id>/')
class OneInFacility(Resource):
	def get(self, prosumer_id, facility_id):
		"""
				HTTP method to view details of a prosumer in a facility
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one_in_facility(role_id, facility_id, prosumer_id)
		return resp, code


@prosumer_api.route('/delete-one/<string:prosumer_id>/')
class DeleteProsumer(Resource):
	def delete(self, prosumer_id):
		"""
				HTTP method to delete details of one prosumer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_prosumer(role_id, prosumer_id, destroy=False)
		return resp, code


@prosumer_api.route('/destroy-one/<string:prosumer_id>/')
class DestroyProsumer(Resource):
	def delete(self, prosumer_id):
		"""
				HTTP method to destroy details of one prosumer
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_prosumer(role_id, prosumer_id, destroy=True)
		return resp, code


@prosumer_api.route('/save-sell-order/')
class NewSellOrder(Resource):
	@prosumer_api.expect(sell_order)
	def post(self):
		"""
				HTTP method to assign a sell order to a prosumer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = SellOrderSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.save_sell_order(**new_payload)
		return resp, code


@prosumer_api.route('/save-buy-order/')
class NewBuyOrder(Resource):
	@prosumer_api.expect(buy_order)
	def post(self):
		"""
				HTTP method to assign a buy order to a prosumer
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = BuyOrderSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.save_buy_order(**new_payload)
		return resp, code


@prosumer_api.route('/refresh-prosumer/<string:prosumer_id>/')
class RefreshProsumer(Resource):
	"""
			Api to send a prosumer as well as its all the other prosumers sharing its parent probe to Kafka.
	"""

	def post(self, prosumer_id):
		"""
				HTTP method to send a prosumer as well as its all the other prosumers sharing its parent probe to Kafka.
				@param: prosumer_id: The ID of the prosumer
				@returns: response and status code
		"""
		manager = RefreshManager()
		resp, code = manager.refresh_prosumer(prosumer_id)
		return resp, code


@prosumer_api.route('/refresh-probe/<string:probe_name>/')
class RefreshProbe(Resource):
	"""
			Api to send a prosumer as well as its all the other prosumers sharing its parent probe to Kafka.
	"""

	def post(self, probe_name):
		"""
				HTTP method to send a prosumer as well as its all the other prosumers sharing its parent probe to Kafka.
				@param: probe_name: The ID of the prosumer
				@returns: response and status code
		"""
		manager = RefreshManager()
		resp, code = manager.refresh_probe(probe_name)
		return resp, code


@prosumer_api.route('/all/')
class AllProsumers(Resource):
	def get(self):
		"""
				PRIVATE API to get all prosumers in all facilities
				@param: facility_id: The name of the facility
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.all(page, role_id)
		return resp, code


@prosumer_api.route('/all-in-facility/<string:facility_id>/')
class AllProsumersInAFacility(Resource):
	def get(self, facility_id):
		"""
				PUBLIC API to get all prosumers in a facility
				@param: facility_id: The name of the facility
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.all_in_facility(page, role_id, facility_id)
		return resp, code


@prosumer_api.route('/all-in-subnet/<string:subnet_id>/')
class AllProsumersInASubnet(Resource):
	def get(self, subnet_id):
		"""
				PUBLIC API to get all prosumers in a subnet
				@param: subnet_id: The name of the subnet
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.prosumers_in_subnet(page, role_id, subnet_id)
		return resp, code


@prosumer_api.route('/consumers-in-subnet/<string:subnet_id>/')
class AllConsumersInASubnet(Resource):
	def get(self, subnet_id):
		"""
				PUBLIC API to get all consumers in a subnet
				@param: subnet_id: The name of the subnet
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.consumers_in_subnet(page, role_id, subnet_id)
		return resp, code


@prosumer_api.route('/producers-in-subnet/<string:subnet_id>/')
class AllProducersInASubnet(Resource):
	def get(self, subnet_id):
		"""
				PUBLIC API to get all producers in a subnet
				@param: subnet_id: The name of the subnet
				@returns: response and status code
		"""
		manager = ProsumerManager()
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		resp, code = manager.producers_in_subnet(page, role_id, subnet_id)
		return resp, code


@prosumer_api.route('/unassigned/<string:facility_id>/')
class UnassignedProsumers(Resource):
	def get(self, facility_id):
		"""
				HTTP method to get all prosumers in a facility that have not been assigned to a user
				@param: facility_id: ID of the facility
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.unassigned(role_id, facility_id)
		return resp, code


@prosumer_api.route('/assigned/<string:facility_id>/')
class AssignedProsumers(Resource):
	def get(self, facility_id):
		"""
				HTTP method to create a new prosumer
				@param: facility_id: ID of the facility
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.assigned(role_id, facility_id)
		return resp, code


@prosumer_api.route('/by-type/<string:prosumer_type_id>/')
class ProsumerType(Resource):
	def get(self, prosumer_type_id):
		"""
				HTTP method to retrieve prosumers in all facilities by prosumer type.
				@param: prosumer_type_id: ID of the prosumer type
				@param: facility: ID of the facility
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		manager = ProsumerManager()
		resp, code = manager.get_by_type(role_id, page, prosumer_type_id)
		return resp, code


@prosumer_api.route('/by-type/<string:prosumer_type_id>/<string:facility_id>/')
class ProsumerTypeInFacility(Resource):
	"""
			Api to retrieve prosumers in a facility by prosumer type.
	"""

	def get(self, prosumer_type_id, facility_id):
		"""
				HTTP method to retrieve prosumers in a facility by prosumer type.
				@param: prosumer_type_id: ID of the prosumer type
				@param: facility: ID of the facility
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		manager = ProsumerManager()
		resp, code = manager.get_by_type_in_facility(role_id, page, prosumer_type_id, facility_id)
		return resp, code


@prosumer_api.route('/by-group/<string:prosumer_group_id>/')
class ProsumersInProsumerGroup(Resource):
	"""
			Api to retrieve prosumers in a prosumer group.
	"""

	def get(self, prosumer_group_id):
		"""
				HTTP method to retrieve prosumers in a prosumer group.
				@param: prosumer_group_id: ID of the prosumer group
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		page = request.args.get('page', 1, type=int)
		manager = ProsumerManager()
		resp, code = manager.get_by_prosumer_group(role_id, page, prosumer_group_id)
		return resp, code


@prosumer_api.route('/unassign-all/<string:user_group_id>/')
class UnassignAllFromAllUser(Resource):
	"""
			Api to manage individual users
	"""
	def post(self, user_group_id):
		"""
				HTTP method to remove all the prosumers a user has from him.
				@param: user_group_id: ID of the user_group
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.remove_all_from_user(role_id, user_group_id)
		return resp, code


@prosumer_api.route('/init-prosumer-configuration/<string:prosumer_id>/')
class SaveProsumerConfig(Resource):
	"""
			Api to send a configuration for a prosumer
	"""
	def post(self, prosumer_id):
		"""
				HTTP method to create a new prosumer
				@returns: response and status code
		"""
		user_group_id = request.cookies.get('group_id')
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.init_config_creation(user_group_id, role_id, prosumer_id)
		return resp, code


@prosumer_api.route('/build-prosumer-configuration/<string:prosumer_id>/')
class BuildProsumerConfig(Resource):
	"""
			Api to send a configuration for a prosumer
	"""
	def post(self, prosumer_id):
		"""
				HTTP method to create a new prosumer
				@returns: response and status code
		"""
		role_id = request.cookies.get('role_id')
		manager = ProsumerManager()
		resp, code = manager.config_builder(role_id, prosumer_id)
		return resp, code
