from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError
from api import api
from api.manager import ProsumerGroupManager
from api.schema import (
	ProsumerGroupSchema,
	ProsumerToProsumerGroupSchema,
	ClientAdminProsumerGroupSchema,
	RemoveClientAdminProsumerGroupSchema,
)
from logger import logger


prosumer_group_api = Namespace('Prosumer Group', description='Api for managing prosumer groups.')

prosumer_group = prosumer_group_api.model('NewGroup', {
	'name': fields.String(required=True, description='The name of the prosumer group'),
	'facility_id': fields.String(required=True, description='The ID of the facility.')
})

bulk_prosumer_group = prosumer_group_api.model('BUlkNewGroup', {
	'prosumer_groups': fields.List(fields.Nested(prosumer_group)),
})

prosumer_to_prosumer_group = prosumer_group_api.model('ProsumerToProsumerGroup', {
	'prosumer_id': fields.String(required=True, description='The ID of the prosumer'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the prosumer group.')
})

client_admin_prosumer_group = prosumer_group_api.model('ClientAdminProsumerGroup', {
	'admin_facility': fields.String(required=True, description='The facility ID of the admin.'),
	'client_admin_id': fields.String(required=True, description='The ID of the client admin'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the prosumer group.')
})

remove_client_admin_prosumer_group = prosumer_group_api.model('RemoveClientAdminProsumerGroup', {
	'client_admin_id': fields.String(required=True, description='The ID of the client admin'),
	'prosumer_group_id': fields.String(required=True, description='The ID of the prosumer group.')
})


@prosumer_group_api.route('/new/')
class NewGroup(Resource):
	"""
			Api to create prosumer groups in a facility.
	"""

	@prosumer_group_api.expect(prosumer_group)
	def post(self):
		"""
				API to create prosumer groups in a facility.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.create(role_id, new_payload)
		return resp, code


@prosumer_group_api.route('/bulk-new-group/')
class BulkNewGroup(Resource):
	"""
			Api to create multiple prosumer groups.
	"""

	@prosumer_group_api.expect(bulk_prosumer_group)
	def post(self):
		"""
				API to create multiple prosumer groups.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data

		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.bulk_create(role_id, payload)
		return resp, code


@prosumer_group_api.route('/edit/<string:group_id>/')
class EditGroup(Resource):
	"""
			Api to edit prosumer groups in a facility.
	"""

	@prosumer_group_api.expect(prosumer_group)
	def post(self, group_id):
		"""
			API to edit prosumer groups in a facility.
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['group_id'] = group_id
		new_payload['role_id'] = role_id
		resp, code = manager.edit(**new_payload)
		return resp, code


@prosumer_group_api.route('/one/<string:group_id>/<string:facility_id>/')
class ViewOneGroup(Resource):
	"""
			Api to edit prosumer groups in a facility.
	"""

	def get(self, group_id, facility_id):
		"""
			API to get one prosumer group in a facility.
			@returns: response and status code
		"""
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_one(group_id, facility_id, role_id)
		return resp, code


@prosumer_group_api.route('/all/<string:facility_id>/')
class ViewAllGroups(Resource):
	"""
			Api to get all prosumer groups in a facility.
	"""

	def get(self, facility_id):
		"""
			API to get all prosumer groups in a facility.
			@returns: response and status code
		"""
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.get_all(facility_id, role_id)
		return resp, code


@prosumer_group_api.route('/assign-prosumer/')
class AssignProsumerToGroup(Resource):
	"""
			Api to add / assign one prosumer to a prosumer group.
	"""

	@prosumer_group_api.expect(prosumer_to_prosumer_group)
	def post(self):
		"""
				Route to add / assign one prosumer to a prosumer group.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerToProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.add_prosumer_to_prosumer_group(**new_payload)
		return resp, code


@prosumer_group_api.route('/remove-prosumer/')
class RemoveProsumerFromGroup(Resource):
	"""
			Api for removing one prosumer from a prosumer group.
	"""

	@prosumer_group_api.expect(prosumer_to_prosumer_group)
	def post(self):
		"""
				Route for removing one prosumer from a prosumer group.
				@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ProsumerToProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.remove_prosumer_from_prosumer_group(**new_payload)
		return resp, code


@prosumer_group_api.route('/add-client-admin/')
class AddClientAdminToProsumerGroup(Resource):
	"""
			Api to assign client admins to prosumer groups.
	"""

	@prosumer_group_api.expect(client_admin_prosumer_group)
	def post(self):
		"""
			API to assign client admins to prosumer groups.
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = ClientAdminProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.add_client_admin(**new_payload)
		return resp, code


@prosumer_group_api.route('/remove-client-admin/')
class RemoveClientAdminFromProsumerGroup(Resource):
	"""
			Api to remove client admins from prosumer groups.
	"""

	@prosumer_group_api.expect(remove_client_admin_prosumer_group)
	def post(self):
		"""
			API to remove client admins from prosumer groups.
			@returns: response and status code
		"""
		data = request.values.to_dict()
		payload = api.payload or data
		schema = RemoveClientAdminProsumerGroupSchema(strict=True)
		try:
			new_payload = schema.load(payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['errors'] = e.messages
			return response, 400

		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		new_payload['role_id'] = role_id
		resp, code = manager.remove_client_admin(**new_payload)
		return resp, code


@prosumer_group_api.route('/delete-one/<string:prosumer_group_id>/')
class DeleteProsumerGroup(Resource):
	def delete(self, prosumer_group_id):
		"""
				HTTP method to delete details of one prosumer
				@returns: response and status code
		"""
		manager = ProsumerGroupManager()
		role_id = request.cookies.get('role_id')
		resp, code = manager.delete_one(role_id, prosumer_group_id)
		return resp, code
