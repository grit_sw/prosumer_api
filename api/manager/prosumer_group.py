from logger import logger
from api.models import ProsumerGroup, ProsumerClientAdmins


class ProsumerGroupManager(object):

	def create(self, role_id, payload):
		response = {}
		name = payload['name']
		facility_id = payload['facility_id']
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		exists = ProsumerGroup.get_one(name=name, facility_id=facility_id)
		if exists:
			response['success'] = False
			response['message'] = 'Prosumer Group {} already exists.'.format(name.title())
			return response, 409

		new_group = ProsumerGroup.create(name.title(), facility_id)
		if new_group:
			response['success'] = True
			response['data'] = new_group
			return response, 201

		response['success'] = False
		response['message'] = 'Prosumer Group {} not created.'.format(name.title())
		return response, 400

	def bulk_create(self, role_id, payload):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		new_prosumer_groups = []

		prosumer_groups = payload['prosumer_groups']
		for prosumer_group in prosumer_groups:
			name = prosumer_group['name']
			facility_id = prosumer_group['facility_id']
			exists = ProsumerGroup.get_one(name=name, facility_id=facility_id)
			if exists:
				new_prosumer_groups.append(exists.to_dict())
				continue

			new_group = ProsumerGroup.create(name.title(), facility_id)
			if new_group:
				new_prosumer_groups.append(new_group)

		response['success'] = True
		response['data'] = new_prosumer_groups
		return response, 201

	def edit(self, name, group_id, facility_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		exists = ProsumerGroup.get_one(name=name, facility_id=facility_id)
		if exists:
			response['success'] = False
			response['message'] = 'Prosumer Group {} already exists.'.format(name.title())
			return response, 409

		prosumer_group = ProsumerGroup.get_one(group_id, facility_id)
		if prosumer_group:
			data = prosumer_group.edit(name)
			if data:
				response['success'] = True
				response['data'] = data
				return response, 201
			response['success'] = False
			response['message'] = 'Error when editing prosumer group. Please try again later.'
			return response, 400

		response['success'] = False
		response['message'] = 'Prosumer group not found.'
		return response, 404

	def get_one(self, group_id, facility_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		if group_id is None:
			response['success'] = False
			response['message'] = 'Prosumer group ID required.'
		prosumer_group = ProsumerGroup.get_one(group_id, facility_id)
		if prosumer_group:
			response['success'] = True
			response['data'] = prosumer_group.get_data()
			return response, 200
		response['success'] = False
		response['message'] = 'Prosumer group not found.'
		return response, 404

	def get_all(self, facility_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_groups = ProsumerGroup.get_all(facility_id)
		if prosumer_groups:
			response['success'] = True
			response['data'] = prosumer_groups
			return response, 200
		else:
			response['success'] = False
			response['message'] = 'No Prosumer groups found.'
			return response, 404

	def add_prosumer_to_prosumer_group(self, prosumer_id, prosumer_group_id, role_id):
		# prosumer_facility is the facility ID of the prosumer
		# group_facility is the facility ID of the prosumer group
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request'
			return response, 400

		prosumer = ProsumerGroup.get_one_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found.'
			return response, 404

		prosumer_group = ProsumerGroup.get_one(group_id=prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Meter group not found'
			return response, 404


		if prosumer.facility_id != prosumer_group.facility_id:
			response['success'] = False
			response['message'] = 'Meter and meter group facility do not match.'
			return response, 403

		if prosumer_group.has_prosumer(prosumer):
			response['success'] = False
			response['message'] = 'Meter {} already assigned to this group.'.format(
				prosumer.name
			)
			return response, 409

		added = prosumer_group.add_prosumer(prosumer)
		if added:
			response['success'] = True
			response['data'] = added
			return response, 200
		response['success'] = False
		response['message'] = 'Error when adding prosumer to prosumer group. Please try again later.'
		return response, 400

	def remove_prosumer_from_prosumer_group(self, prosumer_id, prosumer_group_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer = ProsumerGroup.get_one_prosumer(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 404
		prosumer_group = ProsumerGroup.get_one(group_id=prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Meter group not found'
			return response, 404

		if not prosumer_group.has_prosumer(prosumer):
			response['success'] = False
			response['message'] = 'Meter {} not assigned to meter group {}.'.format(
				prosumer.name, prosumer_group.name
			)
			return response, 404

		data = prosumer_group.remove_prosumer(prosumer)
		response['success'] = True
		response['data'] = data
		return response, 200

	def add_client_admin(self, admin_facility, client_admin_id, prosumer_group_id, role_id):
		# admin_facility is the facility ID of the client admin
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			if int(role_id) < 4:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403

		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_group = ProsumerGroup.get_one(group_id=prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Meter group not found'
			return response, 404

		if prosumer_group.facility_id != admin_facility:
			response['success'] = False
			response['message'] = 'Meter group and admin must be in the same facility.'
			return response, 404

		client_admin = ProsumerClientAdmins.get_one(client_admin=client_admin_id)
		if client_admin:
			if prosumer_group.has_client_admin(client_admin):
				response['success'] = False
				response['message'] = 'Admin already assigned to meter group {}.'.format(
					prosumer_group.name
				)
				return response, 409

		if not client_admin:
			client_admin = ProsumerClientAdmins.create(client_admin=client_admin_id)
		added = prosumer_group.add_client_admin(client_admin)
		if added:
			response['success'] = True
			response['data'] = added
			return response, 200
		response['success'] = False
		response['message'] = 'Error when assigning client admin to meter group. Please try again later.'
		return response, 400

	def remove_client_admin(self, client_admin_id, prosumer_group_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 4:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_group = ProsumerGroup.get_one(group_id=prosumer_group_id)
		if prosumer_group is None:
			response['success'] = False
			response['message'] = 'Prosumer group not found'
			return response, 404

		client_admin = ProsumerClientAdmins.get_one(client_admin=client_admin_id)

		if not client_admin:
			response['success'] = False
			response['message'] = 'Client admin not found'
			return response, 404

		if not prosumer_group.has_client_admin(client_admin):
			response['success'] = False
			response['message'] = 'Admin not assigned to prosumer group {}.'.format(
				prosumer_group.name
			)
			return response, 404

		removed = prosumer_group.remove_client_admin(client_admin)
		if removed:
			response['success'] = True
			response['data'] = removed
			return response, 200

		response['success'] = False
		response['message'] = 'Error when removing client admin from prosumer group. Please try again later.'
		return response, 400


	def delete_one(self, role_id, prosumer_group_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		group = ProsumerGroup.get_one(group_id=prosumer_group_id)
		if not group:
			response['success'] = False
			response['message'] = 'Not found.'
			return response, 404

		name = group.name
		group.delete()
		response['success'] = False
		response['message'] = f'Prosumer Group {name.title()} not created.'
		return response, 204
