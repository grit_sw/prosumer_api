from api.models import ProsumerType, ProsumerClass
from logger import logger


class ProsumerTypeManager(object):
	def create(self, name, class_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if int(role_id) < 5:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_class = ProsumerClass.get_one(class_id=class_id)
		if not prosumer_class:
			response['success'] = False
			response['message'] = 'Prosumer class not found.'
			return response, 404

		exists = ProsumerType.get_one(name=name)
		if exists:
			response['success'] = False
			response['message'] = 'Prosumer Type {} already exists.'.format(name.title())
			return response, 409

		new_prosumer_type = ProsumerType.create(name, prosumer_class)
		if new_prosumer_type:
			response['success'] = True
			response['data'] = new_prosumer_type
			return response, 201
		response['success'] = False
		response['message'] = 'Error in Creating Prosumer {}'.format(name.title())
		return response, 400

	def edit(self, name, type_id, class_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if not int(role_id) >= 5:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_type = ProsumerType.get_one(type_id=type_id)
		if not prosumer_type:
			response['success'] = False
			response['message'] = 'Prosumer type not found.'
			return response, 404

		dupe_type = ProsumerType.get_one(name=name)
		if dupe_type:
			response['success'] = False
			response['message'] = f'Prosumer type {name} already exists'
			return response, 409

		prosumer_class = ProsumerClass.get_one(class_id=class_id)
		if not prosumer_class:
			response['success'] = False
			response['message'] = 'Prosumer class not found.'
			return response, 404

		data = prosumer_type.edit(name, prosumer_class)
		if not data:
			response['success'] = False
			response['message'] = 'Service Unavailable. Try again later.'
			return response, 500

		response['success'] = True
		response['data'] = data
		return response, 201

	def get_one(self, type_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_type = ProsumerType.get_one(type_id=type_id)
		if not prosumer_type:
			response['success'] = False
			response['message'] = 'Not found.'
			return response, 404

		response['success'] = True
		response['data'] = prosumer_type.get_data()
		return response, 201

	def by_class(self, class_id, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_class = ProsumerClass.get_one(class_id=class_id)
		if not prosumer_class:
			response['success'] = False
			response['message'] = 'Prosumer class not found.'
			return response, 404

		data = [prosumer_type.to_dict() for prosumer_type in prosumer_class.prosumer_types.all()]
		response['success'] = True
		response['data'] = data
		return response, 201

	def get_all(self, role_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		data = ProsumerType.get_all()
		if not data:
			response['success'] = False
			response['message'] = 'No Prosumer types created.'
			return response, 404

		response['success'] = True
		response['data'] = data
		return response, 200

	def delete_one(self, role_id, type_id):
		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourised.'
			return response, 403
		try:
			if int(role_id) < 3:
				response['success'] = False
				response['message'] = 'Unauthourized.'
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Invalid request.'
			return response, 400

		prosumer_type = ProsumerType.get_one(type_id=type_id)
		if prosumer_type is None:
			response['success'] = False
			response['message'] = 'Prosumer type not found.'
			return response, 404

		try:
			prosumer_type.delete_one()
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unable to delete prosumer type.'
			return response, 500

		response['success'] = True
		response['message'] = 'Prosumer type deleted.'
		return response, 204
