from flask import request

from api.exceptions import KafkaConnectionError
from api.models import Prosumer
from api.models.prosumer import Probe
from logger import logger


class ProsumerManager(object):
	"""Class for prosumer operations"""

	def new_prosumer(self, data):
		response = {}
		role_id = data['role_id']
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		device_id = data['device_id']
		user_group_id = data['user_group_id']
		prosumer_name = data['prosumer_name']
		prosumer = Prosumer.get_one(device_id=device_id)
		user_prosumer = Prosumer.get_one(user_group_id=user_group_id)
		if user_prosumer:
			name_exists = user_prosumer.name_exists(prosumer_name)
			if name_exists:
				response['success'] = False
				response['message'] = f'A meter with name {prosumer_name} is already assigned to this user.'
				return response, 409
		if prosumer:
			if prosumer.user_group_id == user_group_id:
				response['success'] = False
				response['message'] = 'Meter already assigned to this user.'
				return response, 409
			elif prosumer.user_group_id != user_group_id:
				response['success'] = False
				response['message'] = 'Meter already assigned to another user.'
				return response, 409
		del data['role_id']
		resp = Prosumer.create(**data)
		response['success'] = True
		response['data'] = resp
		return response, 201

	def new_bulk_prosumer(self, role_id, data):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		prosumers = data['prosumers']
		new_prosumers = Prosumer.create_bulk(prosumers)
		logger.info(new_prosumers)
		response['success'] = True
		response['data'] = new_prosumers
		return response, 201

	def edit(self, data):
		response = {}
		role_id = data['role_id']
		del data['role_id']
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 4:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		prosumer_id = data['prosumer_id']
		del data['prosumer_id']
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if not prosumer:
			response['success'] = False
			response['message'] = 'Meter not found.'
			return response, 404
		resp = prosumer.edit(**data)
		response['success'] = True
		response['data'] = resp
		return response, 201

	def search(self, search_term, size, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		data, has_prev, has_next = Prosumer.search_for(search_term, size)

		response['success'] = True
		response['data'] = data
		response['has_prev'] = has_prev
		response['has_next'] = has_next
		return response, 200

	def save_sell_order(self, role_id, sellorder_id, prosumer_id):
		response = {}
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 2:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403

		if prosumer.is_a_consumer():
			data = prosumer.get_data()
			response['success'] = False
			response['message'] = f"{data['prosumer_name']} of type {data['prosumer_type']['prosumer_type_name']} cannot sell electricity."
			return response, 403

		if prosumer.sell_order_id == sellorder_id:
			response['success'] = False
			response['message'] = 'Sell order already created.'
			response['data'] = prosumer.get_data()
			return response, 409

		data = prosumer.save_sell_order(sellorder_id)
		if not data:
			logger.warning('Plan ID {} creation for prosumer {} failed'.format(
				sellorder_id, prosumer.prosumer_id))
			response['success'] = False
			response['message'] = 'Service Error'
			return response, 500
		response['success'] = True
		response['message'] = data
		return response, 201

	def save_buy_order(self, role_id, buyorder_id, prosumer_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 2:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403
		if prosumer.is_a_producer():
			data = prosumer.get_data()
			print(data)
			response['success'] = False
			response['message'] = f"{data['prosumer_name']} of type {data['prosumer_type']['prosumer_type_name']} cannot buy electricity."
			return response, 403

		if prosumer.buy_order_id == buyorder_id:
			response['success'] = False
			response['message'] = 'Buy order already created.'
			response['data'] = prosumer.get_data()
			return response, 409

		data = prosumer.save_buy_order(buyorder_id)
		if not data:
			logger.warning('Plan ID {} creation for prosumer {} failed'.format(
				buyorder_id, prosumer.prosumer_id))
			response['success'] = False
			response['message'] = 'Service Error'
			return response, 500
		response['success'] = True
		response['message'] = data
		return response, 201

	def my_prosumers(self, page, user_id, group_id, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		manager = ProsumerManager()
		if role_id >= 5:
			response, status_code = manager.all(page, role_id)
			return response, status_code
		if role_id == 3:
			data, has_prev, has_next = Prosumer.client_admin_prosumers(page, user_id)
			response['success'] = True
			response['count'] = len(data)
			response['data'] = data
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200
		response, status_code = manager.user_prosumers(page, group_id)
		return response, status_code

	def user_prosumers(self, page, user_group_id):
		response = {}
		prosumer = Prosumer.get_one(user_group_id=user_group_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'This user has no meters.'
			return response, 404
		try:
			prosumers, has_prev, has_next = Prosumer.user_prosumers(
				page, user_group_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def my_info(self, user_group_id):
		response = {}
		prosumer = Prosumer.get_one(user_group_id=user_group_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'This user has no meters.'
			return response, 404
		try:
			data = prosumer.my_info()
			response['success'] = True
			response['data'] = data
			return response, 200
		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def all(self, page, role_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
				# This is a compromise done for speed of getting an api to production.
				# Correct this by studying how roles are managed in microservices
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			prosumers, has_prev, has_next = Prosumer.all(page)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def all_in_facility(self, page, role_id, facility_id):
		response = {}
		try:
			role_id = int(role_id)
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'User not found'
			return response, 403
		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.all_in_facility(
				page, facility_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 200

		except Exception as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def unassigned(self, role_id, facility_id):
		response = {}
		try:
			role_id = int(role_id)
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'User not found'
			return response, 403
		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		response = {}
		if not Prosumer.get_by_facility(facility_id=facility_id):
			response['success'] = False
			response['message'] = 'Facility not found.'
			return response, 404

		data = Prosumer.unassigned(facility_id)
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		return response, 200

	def assigned(self, role_id, facility_id):
		response = {}
		try:
			role_id = int(role_id)
		except (ValueError, TypeError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'User not found'
			return response, 403
		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not Prosumer.get_by_facility(facility_id=facility_id):
			response['success'] = False
			response['message'] = 'Facility Not Found.'
			return response, 404

		data = Prosumer.assigned_prosumers(facility_id)
		response['success'] = True
		response['count'] = len(data)
		response['data'] = data
		return response, 200

	def get_one(self, prosumer_id):
		response = {}
		try:
			prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		except AttributeError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403

		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403
		response['success'] = True
		response['data'] = prosumer.to_dict()
		return response, 200

	def get_one_in_facility(self, role_id, facility_id, prosumer_name):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if not int(role_id) >= 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403
		try:
			prosumer = Prosumer.get_one(name=prosumer_name)
		except AttributeError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403

		if facility_id != prosumer.facility_id:
			response['success'] = False
			response['message'] = 'Meter in {} not found in this facility.'.format(prosumer.facility_id)
			return response, 404

		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403
		response['success'] = True
		response['data'] = prosumer.to_dict()
		return response, 200

	def get_by_type(self, role_id, page, type_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id <= 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.get_prosumer_by_type(page, type_id=type_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def get_by_type_in_facility(self, role_id, page, type_id, facility_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id <= 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.get_prosumer_by_type_by_facility(page, type_id=type_id, facility_id=facility_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def get_by_prosumer_group(self, role_id, page, prosumer_group_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.get_prosumer_by_prosumer_group(page, prosumer_group_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. Team has been notified. Please try again later'
			return response, 500

	def prosumers_in_subnet(self, page, role_id, subnet_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.prosumers_in_subnet(page, subnet_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. The team has been notified. Please try again later'
			return response, 500

	def consumers_in_subnet(self, page, role_id, subnet_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.consumers_in_subnet(page, subnet_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. The team has been notified. Please try again later'
			return response, 500

	def producers_in_subnet(self, page, role_id, subnet_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			prosumers, has_prev, has_next = Prosumer.producers_in_subnet(page, subnet_id)
			response['success'] = True
			response['count'] = len(prosumers)
			response['data'] = prosumers
			response['has_prev'] = has_prev
			response['has_next'] = has_next
			return response, 201
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Error during request. The team has been notified. Please try again later'
			return response, 500

	def delete_prosumer(self, role_id, prosumer_id, destroy=False):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		not_destroyed = not destroy
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id, deleted=destroy)
		prosumer_ = Prosumer.get_one(prosumer_id=prosumer_id, deleted=not_destroyed)
		# allows prosumer deletion if it has been marked for deletion but not yet deleted
		prosumer = prosumer or prosumer_
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter {} not found.'.format(prosumer_id)
			return response, 404

		# facilities = request.cookies.get('facilities')
		# if not isinstance(facilities, list):
		# 	response['success'] = False
		# 	response['message'] = 'Unauthourized'
		# 	return response, 403

		# if prosumer.facility_id not in set(facilities):
		# 	response['success'] = False
		# 	response['message'] = 'Unauthourized'
		# 	return response, 403

		prosumer_name = prosumer.name.replace('_', ' ').title()
		try:
			prosumer.delete_one(destroy=destroy)
		except KafkaConnectionError as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service temporarily unavailable.'
			return response, 400
		response['success'] = True
		response['message'] = 'Meter deleted {}'.format(prosumer_name)
		return response, 200

	def remove_all_from_user(self, role_id, user_group_id):
		"""Transaction Engine method to remove all prosumers in user group from the user."""

		response = {}
		if not role_id:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 5:
			# If request isn't made from cmdctrl aka transaction engine.
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_one(user_group_id=user_group_id)
		if not prosumer:
			response['success'] = False
			response['message'] = 'This user has no meters.'
			return response, 404

		try:
			Prosumer.remove_all_from_user_group(user_group_id)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Service currently unavailable. Please try again later.'
			return response, 500

		response['success'] = True
		return response, 201

	def init_config_creation(self, user_group_id, role_id, prosumer_id):
		"""Start the process of configuring a prosumer."""
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403

		if (not prosumer.user_group_id == user_group_id) or (role_id > 3):
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer.start_prosumer_config()
		response['success'] = True
		response['message'] = 'Meter configuration commenced.'
		return response, 201

	def config_builder(self, role_id, prosumer_id):
		response = {}
		try:
			role_id = int(role_id)
		except (TypeError, ValueError) as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		if role_id < 3:
			response['success'] = False
			response['message'] = 'Unauthourized'
			return response, 403

		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found'
			return response, 403

		response['success'] = True
		response['data'] = prosumer.prepare_probe_config()
		return response, 200


class RefreshManager(object):

	def refresh_prosumer(self, prosumer_id):
		response = {}
		prosumer = Prosumer.get_one(prosumer_id=prosumer_id)
		if prosumer is None:
			response['success'] = False
			response['message'] = 'Meter not found.'
			return response, 404
		prosumer.send_prosumer_to_kafka()
		response['success'] = True
		response['message'] = 'Refreshed meters.'
		return response, 201

	def refresh_probe(self, probe_name):
		response = {}
		probe = Probe.get_one(probe_name)
		if probe is None:
			response['success'] = False
			response['message'] = 'Probe not found.'
			return response, 404
		probe.send_to_kafka()
		response['success'] = True
		response['message'] = 'Refreshed meters.'
		return response, 201
