import os

from flask import Flask
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy

from config import config
from logger import logger

db = SQLAlchemy()
api = Api(doc='/doc/')

app = Flask(__name__)


def create_api(config_name):

	try:
		init_config = config[config_name]()
	except KeyError:
		logger.exception(e)
		raise

	except Exception as e:
		logger.exception(e)
		raise

	logger.info('Running in {} Mode'.format(init_config))

	app_config = config.get(config_name)
	app.config.from_object(app_config)

	if config_name not in {'Development', 'default', 'docs'}:
		app_config.init_app(app)
	db.init_app(app)

	from api.controllers import prosumer_api as ns1
	from api.controllers import prosumer_group_api as ns2
	# from api.controllers import prosumer_switch_api as ns3
	from api.controllers import prosumer_type_api as ns4
	# from api.controllers import prosumer_class_api as ns5

	# Prosumer
	api.add_namespace(ns1, path='/prosumers')
	# Prosumer Group
	api.add_namespace(ns2, path='/prosumer-group')
	# # Prosumer switching
	# api.add_namespace(ns3, path='/prosumer-switch')
	# Prosumer type
	api.add_namespace(ns4, path='/prosumer-type')
	# # Prosumer class
	# api.add_namespace(ns5, path='/prosumer-class')

	api.init_app(app)

	with app.app_context():
		# db.reflect()
		# db.drop_all()
		db.create_all()

	return app
