from collections import namedtuple

from marshmallow import Schema, ValidationError, fields, post_load, validates

SellOrder = namedtuple('SellOrder', [
	'prosumer_id',
	'sellorder_id',
])

BuyOrder = namedtuple('BuyOrder', [
	'prosumer_id',
	'buyorder_id',
])

NewProsumer = namedtuple('NewProsumer', [
	'prosumer_name',
	'user_group_id',
	'device_id',
	'configuration_id',
	'prosumer_url',
	'prosumer_type_id',
	'prosumer_group_id',
	'facility_id',
	'subnet_ids',
	'probe_names',
])

BulkNewProsumer = namedtuple('BulkNewProsumer', [
	'prosumers',
])


class SellOrderSchema(Schema):
	prosumer_id = fields.String(required=True)
	sellorder_id = fields.String(required=True)

	@post_load
	def assign(self, data):
		return SellOrder(**data)

	@validates('sellorder_id')
	def validate_sellorder_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Sell order required.')

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer required.')


class BuyOrderSchema(Schema):
	prosumer_id = fields.String(required=True)
	buyorder_id = fields.String(required=True)

	@post_load
	def assign(self, data):
		return BuyOrder(**data)

	@validates('buyorder_id')
	def validate_buyorder_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Buy order required.')

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer required.')


class NewProsumerSchema(Schema):
	prosumer_name = fields.String(required=True)
	user_group_id = fields.String(required=True)
	configuration_id = fields.String(required=False, allow_none=True)
	device_id = fields.String(required=True)
	prosumer_url = fields.String(required=True)
	prosumer_type_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)
	facility_id = fields.String(required=True)
	subnet_ids = fields.List(fields.String(required=True))
	probe_names = fields.List(fields.String(required=True))

	@post_load
	def create_prosumer(self, data):
		return NewProsumer(**data)

	@validates('prosumer_name')
	def validate_prosumer_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Probe ID required.')

	@validates('user_group_id')
	def validate_user_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Probe ID required.')

	@validates('device_id')
	def validate_device_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Device ID required.')

	@validates('prosumer_url')
	def validate_device_id_url(self, value):
		if len(value) <= 0:
			raise ValidationError('Decive ID URL required.')

	@validates('prosumer_type_id')
	def validate_prosumer_type_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID required.')

	@validates('facility_id')
	def validate_facility_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID required.')

class BulkNewProsumerSchema(Schema):
	prosumers = fields.Nested('NewProsumerSchema', many=True, strict=True)

	@post_load
	def create_prosumer(self, data):
		return BulkNewProsumer(**data)
