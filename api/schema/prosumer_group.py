from marshmallow import Schema, fields, post_load, validates, ValidationError
from collections import namedtuple

NewProsumerGroup = namedtuple('NewProsumerGroup', [
	'name',
	'facility_id',
])

ProsumerToProsumerGroup = namedtuple('ProsumerToProsumerGroup', [
	'prosumer_id',
	'prosumer_group_id',
])

ClientAdminProsumerGroup = namedtuple('ClientAdminProsumerGroup', [
	'admin_facility',
	'client_admin_id',
	'prosumer_group_id',
])

RemoveClientAdmin = namedtuple('RemoveClientAdmin', [
	'client_admin_id',
	'prosumer_group_id',
])


class ProsumerGroupSchema(Schema):
	name = fields.String(required=True)
	facility_id = fields.String(required=True)

	@post_load
	def new_prosumer(self, data):
		return NewProsumerGroup(**data)

	@validates('name')
	def validate_name(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer group required.')

	@validates('facility_id')
	def validate_facility_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility required.')

class ProsumerToProsumerGroupSchema(Schema):
	prosumer_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)

	@post_load
	def prosumer_group(self, data):
		return ProsumerToProsumerGroup(**data)

	@validates('prosumer_id')
	def validate_prosumer_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Group required.')


class ClientAdminProsumerGroupSchema(Schema):
	admin_facility = fields.String(required=True)
	client_admin_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)

	@post_load
	def client_admin(self, data):
		return ClientAdminProsumerGroup(**data)

	@validates('client_admin_id')
	def validate_client_admin_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Client Admin required.')

	@validates('admin_facility')
	def validate_admin_facility(self, value):
		if len(value) <= 0:
			raise ValidationError('Facility ID of the admin is required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Group required.')


class RemoveClientAdminProsumerGroupSchema(Schema):
	client_admin_id = fields.String(required=True)
	prosumer_group_id = fields.String(required=True)

	@post_load
	def client_admin(self, data):
		return RemoveClientAdmin(**data)

	@validates('client_admin_id')
	def validate_client_admin_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Client Admin required.')

	@validates('prosumer_group_id')
	def validate_prosumer_group_id(self, value):
		if len(value) <= 0:
			raise ValidationError('Prosumer Group required.')
