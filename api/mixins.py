from time import sleep

from confluent_kafka.avro.error import ClientError
from flask import current_app

from api.exceptions import KafkaConnectionError
from logger import logger

# from requests.exceptions import ConnectionError



class KafkaMixin(object):

	@staticmethod
	def notify_kafka(payload):

		kafka_topic = payload['kafka_topic']
		use_avro = payload['use_avro']
		del payload['kafka_topic']
		del payload['use_avro']

		retry_count = 0
		try:
			while True:
				try:
					current_app.kafka_producer.send_message(
						topic=kafka_topic,
						message=payload,
						use_avro=use_avro
					)
					break
				except (ClientError) as e:
					logger.info("KAFKA PAYLOAD = {}".format(payload))
					logger.exception(e)
					retry_count += 1
					sleep(5)
					if retry_count == 5:
						raise KafkaConnectionError(
							'Unable to send message to Kafka broker after {} retries.'.format(retry_count)
						)
			logger.info('Produced message = {} to topic {}'.format(payload, kafka_topic))
		except KafkaConnectionError as e:
			logger.critical(e)

	@classmethod
	def before_commit(cls, session):
		added_items = []
		updated_items = []
		deleted_items = []
		session.changes = {}
		# print(f"\n\t\t session.new = {session.new}")
		# print(f"\n\t\t session.dirty = {session.dirty}")
		# print(f"\n\t\t session.deleted = {session.deleted}")
		kafka_topic = current_app.config.get('PROSUMER_UPDATE_TOPIC')
		for model in list(session.new):
			if model.__tablename__ in {'prosumer_group'}:
				prosumers = model.prosumers.all()
				added_items.extend([prosumer.kafka_dict(kafka_topic) for prosumer in prosumers])
			if model.__tablename__ == 'prosumer':
				kafka_topic = current_app.config.get('NEW_PROSUMER_TOPIC')
				# print(f"\n\n\n\t\t New KAFKA TOPIC = {kafka_topic}")
				added_items.extend([model.kafka_dict(kafka_topic)])

		for model in list(session.dirty):
			if model.__tablename__ in {'prosumer_group'}:
				prosumers = model.prosumers.all()
				updated_items.extend([prosumer.kafka_dict(kafka_topic) for prosumer in prosumers])
			if model.__tablename__ == 'prosumer':
				if model.deleted:
					kafka_topic = current_app.config.get('PROSUMER_DELETED_TOPIC')
					updated_items.extend([model.kafka_dict(kafka_topic)])
					continue
				kafka_topic = current_app.config.get('PROSUMER_UPDATE_TOPIC')
				# print(f"\n\n\n\t\t Dirty KAFKA TOPIC = {kafka_topic}")
				updated_items.extend([model.kafka_dict(kafka_topic)])

		for model in list(session.deleted):
			if model.__tablename__ == 'prosumer':
				deleted_items.append(model.kafka_dict(kafka_topic))

		session.changes = {
			'add': added_items,
			'update': updated_items,
			'delete': deleted_items
		}
		# print(session.changes)

	@classmethod
	def after_commit(cls, session):
		if current_app.config['ENV'] not in {'Development', 'defualt'}:
			if session.changes:
				for data in session.changes['add']:
					if data:
						KafkaMixin.notify_kafka(data)
				for data in session.changes['update']:
					if data:
						KafkaMixin.notify_kafka(data)
				for data in session.changes['delete']:
					if data:
						KafkaMixin.notify_kafka(data)
			session.changes = None
