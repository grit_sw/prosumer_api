# Test Server.
from os import environ

from arrow import now
from flask import request
from flask_migrate import Migrate

from api import create_api, db
from logger import logger

if environ.get('FLASK_ENV') is None:
	print('FLASK_ENV not set')
mode = environ.get('FLASK_ENV') or 'Staging'
app = create_api(mode)
migrate = Migrate(app, db)

@app.before_first_request
def set_up():
	with app.app_context():
		from api.models import ProsumerClass, ProsumerType
		ProsumerClass.init_types()
		ProsumerType.init_types()
		if not mode in {'production', 'Production'}:
			from pprint import pprint
			pprint(ProsumerType.get_all())
			pprint(ProsumerClass.get_all())

@app.after_request
def log_info(response):
	try:
		log_data = {
			'connection': request.headers.get('Connection'),
			'ip_address': request.remote_addr,
			'browser_name': request.user_agent.browser,
			'user_device': request.user_agent.platform,
			'referrer': request.referrer,
			'request_url': request.url,
			'host_url': request.host_url,
			'status_code': response.status_code,
			'date': str(now('Africa/Lagos')),
			'location': response.location,
		}
		logger.info('prosumer_api_logs : {}'.format(log_data))
	except Exception as e:
		logger.exception("prosumer_api_logs: {}".format(e))
	return response

with app.app_context():
	# db.reflect()
	# db.drop_all()
	db.create_all()
	# upgrade()

# set_up()

# if __name__ == '__main__':
#     app.run()
